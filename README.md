Вариант 20

Выполнила: Мамышева Валерия, КИ22-16/1Б 

**Задача**: Написать функцию, реализующую вычисление сочетаний с повторениями для переданной последовательности

## Математическая справка
**Сочетания с повторениями** — комбинаторные соединения из n элементов по m, составленные из этих элементов без учета порядка с возможностью многократного повторения элементов.

*Пример*
Пусть задано 5 различных элементов a, b, c, d, e (в достаточном количестве комплектов) и пусть требуется составить из этих пяти элементов по 3 элемента сочетания с повторениями.
Если бы сочетания составлялись без повторений, то они должны были быть различными: abc abd abe acd ace ade bcd bce bde cde.
Сочетания же с повторениями по три элемента из заданных пяти элементов будут иметь вид:
* aaa, aab, aac, aad, aae, abb, abc, abd,abe, acb, acd, ace, add, ade, aee,
* bbb, bbc, bbd, bbe, bce, bcd, bcе, bdd, bde, bee, 
* ccc, ccd, ccе, cdd, cde, ceе, ddd, dde, dee, eee.

Подробнее про метод можно прочитать [на сайте файлового архива студентов](https://studfile.net/preview/7220017/page:7/)

![формула сочетания с повторениями](https://studfile.net/html/2706/260/html_SzxY9H7kX9.6oER/img-TVBXHb.png)

пример: $`n1 · n2 · n3 = 20 · 19 · 18 = 6 840 способов`$

Процесс работы программы:
* [ ] Пользователь вводит поочередно числа в кортеж,
* [ ] Пользователь вводит число элементов для итерации,
* [ ] Кортеж и число элементов являются аргументами функции, вычисляющей кортеж сочетания с повторениями.

## Examples:
    >>>result = combinations_with_repetitions((1,2,3), 2)
    >>>print(list(result))
    [(1, 2), (1, 3), (2, 2), (2, 3), (3, 3)]

    >>>result = combinations_with_repetitions(124, 2)
    >>>print(list(result))  
    TypeError: object of type 'int' has no len()